package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.model.Employee;
import cc.techport.web.nailcashier.model.Good;
import cc.techport.web.nailcashier.model.Order;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

public interface OrderBusiness {

    class PurchaseOrderInfo{
        private Integer memberId;
        private Integer employeeId;
        private List<Good> orderGoods;

        public Integer getMemberId() {
            return memberId;
        }

        public void setMemberId(Integer memberId) {
            this.memberId = memberId;
        }

        public Integer getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(Integer employeeId) {
            this.employeeId = employeeId;
        }

        public List<Good> getOrderGoods() {
            return orderGoods;
        }

        public void setOrderGoods(List<Good> orderGoods) {
            this.orderGoods = orderGoods;
        }
    }

    Map<String,Object> createPurchaseOrder(PurchaseOrderInfo orderInfo);

    class FundOrderInfo{
        private Integer memberId;
        private String memberName;
        private List<Employee> employees;
        private Double cash;
        private Double balance;

        public Integer getMemberId() {
            return memberId;
        }

        public void setMemberId(Integer memberId) {
            this.memberId = memberId;
        }

        public String getMemberName() {
            return memberName;
        }

        public void setMemberName(String memberName) {
            this.memberName = memberName;
        }

        public List<Employee> getEmployees() {
            return employees;
        }

        public void setEmployees(List<Employee> employees) {
            this.employees = employees;
        }

        public Double getCash() {
            return cash;
        }

        public void setCash(Double cash) {
            this.cash = cash;
        }

        public Double getBalance() {
            return balance;
        }

        public void setBalance(Double balance) {
            this.balance = balance;
        }
    }

    Map<String,Object> createFundOrder(FundOrderInfo fundInfo);

    Map<String,Object> revokeOrder(Order order);

    Map<String,Object> updateOrderStatus(Order order);

    Map<String,Object> updateOrderComments(Order order);

    Map<String,Object> getBusinessReport(String startTime, String endTime);

    Map<String,Object> getOrderByTime(String startTime, String endTime);

    Map<String,Object> getOrderByMember(String startTime, String endTime, Integer memberId);

    class QueryOrderInfo {
        private String startTime;
        private String endTime;
        private String orderType;
        private Integer memberId;
        private Integer employeeId;

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public Integer getMemberId() {
            return memberId;
        }

        public void setMemberId(Integer memberId) {
            this.memberId = memberId;
        }

        public Integer getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(Integer employeeId) {
            this.employeeId = employeeId;
        }
    }

    Map<String,Object> queryOrder(QueryOrderInfo queryInfo);

}
