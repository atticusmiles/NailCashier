package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.model.User;
import cc.techport.web.nailcashier.service.UserService;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import cc.techport.web.nailcashier.utils.SecurityUtil;
import cc.techport.web.nailcashier.utils.ValidateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class UserBusinessImpl implements UserBusiness {

    public static final Log LOGGER = LogFactory.getLog(UserBusinessImpl.class);
    @Autowired
    private UserService userService;

    @Override
    public Map<String, Object> createUser(User user) {

        LOGGER.info("Creating user:" + user);
        try {
            if(!ValidateUtil.isWord(user.getUsername())){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Please enter a valid username.").asMap();
            }

            if(StringUtils.isBlank(user.getPassword())){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Please enter a password.").asMap();
            }

            String encrytedPassword = SecurityUtil.makeMD5(user.getPassword());
            user.setPassword(encrytedPassword);

            userService.createUser(user);

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Successful created user.").asMap();
        } catch (Exception e) {
            LOGGER.error("Error creating user.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Error creating user.").asMap();
        }
    }

    @Override
    public Map<String, Object> updateUserInfo(User user) {
        try {
            if(null == user.getUserId()){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("User should have an userId.").asMap();
            }

            if(null == userService.getUserByUserId(user.getUserId())){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_STATE)
                        .respMsg("User does not exist.").asMap();
            }

            userService.updateUserInfo(user);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Successful updating user.").asMap();
        } catch (Exception e) {
            LOGGER.error("Error updating user info.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Error updating user info.").asMap();
        }
    }

    @Override
    public Map<String, Object> updateUserPwd(User user) {
        try {
            if(null == user.getUserId()){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("User should have an userId.").asMap();
            }

            String plainPwd = user.getPassword();
            user.setPassword(SecurityUtil.makeMD5(plainPwd));
            userService.updateUserPwd(user);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Successful updating user password.").asMap();
        } catch (Exception e) {
            LOGGER.error("Error updating user password.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Error updating user password.").asMap();
        }
    }


    @Override
    public Map<String, Object> deleteUser(User user) {
        try {
            if(null == user.getUserId()){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("User should have an userId.").asMap();
            }
            userService.deleteUser(user);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Successful deleting user.").asMap();
        } catch (Exception e) {
            LOGGER.error("Error deleting user.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Error deleting user.").asMap();
        }
    }

    @Override
    public Map<String, Object> getAllUsers() {
        try {
            List<User> userList = userService.getAllUsers();
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Successful fetched all users").property("users", userList).asMap();
        } catch (Exception e) {
            LOGGER.error("Error fetching all users", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Error fetching all users").asMap();
        }
    }

}
