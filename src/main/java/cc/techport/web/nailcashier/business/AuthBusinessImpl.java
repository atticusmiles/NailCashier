package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.model.AuthGroup;
import cc.techport.web.nailcashier.model.User;
import cc.techport.web.nailcashier.service.AuthGroupService;
import cc.techport.web.nailcashier.service.JWTService;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class AuthBusinessImpl implements AuthBusiness {

    public static final Log LOGGER = LogFactory.getLog(AuthBusinessImpl.class);
    @Value(value = "${nailCashier.auth.free.paths}")
    private String freePaths;
    @Autowired
    private JWTService jwtService;
    @Autowired
    private AuthGroupService authGroupService;

    @Override
    public Map<String, Object> createAuthGroup(AuthGroup authGroup) {
        try {
            if (StringUtils.isBlank(authGroup.getGroupName())) {
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("AuthGroup should have a name.").asMap();
            }
            authGroupService.createAuthGroup(authGroup);

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Successfully added auth group").asMap();
        }catch (Exception e){
            LOGGER.error("Error when adding auth group",e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Error when adding auth group").asMap();
        }
    }

    @Override
    public Map<String, Object> deleteAuthGroup(AuthGroup authGroup) {
        if (null == authGroup.getGroupId()) {
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                    .respMsg("AuthGroup should have an id.").asMap();
        }

        authGroupService.deleteAuthGroup(authGroup);

        return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                .respMsg("Successfully deleted auth group").asMap();
    }

    @Override
    public Map<String, Object> updateAuthGroup(AuthGroup authGroup) {
        try {
            if(null == authGroup.getGroupId()){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("User should have an userId.").asMap();
            }

            if(null == authGroupService.getAuthGroupById(authGroup.getGroupId())){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_STATE)
                        .respMsg("Group does not exist.").asMap();
            }

            authGroupService.updateAuthGroup(authGroup);

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Successfully added auth group").asMap();
        }catch (Exception e){
            LOGGER.error("Error when adding auth group",e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Error when adding auth group").asMap();
        }
    }

    @Override
    public Map<String, Object> getAllAuthGroups() {
        try {
            List<AuthGroup> groups = authGroupService.getAllAuthGroups();

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Fetched all groups.")
                    .property("groups",authGroupService.getAllAuthGroups())
                    .asMap();
        }catch (Exception e){
            LOGGER.error("Error when adding auth group",e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Error when adding auth group").asMap();
        }
    }

    @Override
    public boolean isPathFree(String path) {
        List<String> freePathList = Arrays.asList(freePaths.split(","));

        for (String freePath : freePathList) {
            if (path.startsWith(freePath)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String issueKeyForUser(User realUser){
        if (null ==  realUser) {
            return null;
        }
        JWTService.AuthForm authForm = new JWTService.AuthForm();
        authForm.setUserId(realUser.getUserId());
        authForm.setUsername(realUser.getUsername());
        authForm.setAuthedPaths(authGroupService.getAuthGroupByName(realUser.getAuthGroup()).getBackendPaths());
        return jwtService.genAuthKey(authForm);
    }

    @Override
    public boolean isPathAuthedForUser(String path, User user) {
        if (StringUtils.isBlank(path) || null == user || StringUtils.isBlank(user.getUsername())) {
            return false;
        }
        if (isPathFree(path)) {
            return true;
        }
        //free access for user admin
        if (Constants.ADMIN_USERNAME.equals(user.getUsername())) {
            return true;
        }

        //other users
        List<String> authedPaths = Arrays.asList(authGroupService.getAuthGroupByName(user.getAuthGroup()).getBackendPaths().split(","));
        return isPathAuthed(path, authedPaths);
    }

    private boolean isPathAuthed(String pathToVerify, List<String> authedPaths) {
        if (StringUtils.isBlank(pathToVerify)) {
            return false;
        }
        for (String authedPath : authedPaths) {
            if (pathToVerify.startsWith(authedPath)) {
                return true;
            }
        }
        return false;
    }

}
