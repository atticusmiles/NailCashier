package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.model.Employee;
import cc.techport.web.nailcashier.model.Good;

import java.util.Map;

public interface EmployeeBusiness {

    Map<String,Object> createEmployee(Employee employee);

    Map<String,Object> updateEmployee(Employee employee);

    Map<String,Object> deleteEmployee(Employee employee);

    Map<String,Object> getAllEmployees();

    Map<String,Object> getEmployeeById(Employee employee);

}
