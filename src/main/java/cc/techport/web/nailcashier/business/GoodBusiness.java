package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.model.Good;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public interface GoodBusiness {

    Map<String,Object> createGood(Good good);

    Map<String,Object> updateGoodInfo(Good good);

    Map<String,Object> deleteGood(Good good);

    Map<String,Object> getAllGoods();

    Map<String,Object> setGoodStock(Good good);

}
