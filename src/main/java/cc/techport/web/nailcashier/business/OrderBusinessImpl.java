package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.dto.OrderStatus;
import cc.techport.web.nailcashier.dto.OrderType;
import cc.techport.web.nailcashier.model.Employee;
import cc.techport.web.nailcashier.model.Good;
import cc.techport.web.nailcashier.model.Member;
import cc.techport.web.nailcashier.model.Order;
import cc.techport.web.nailcashier.service.EmployeeService;
import cc.techport.web.nailcashier.service.GoodService;
import cc.techport.web.nailcashier.service.MemberService;
import cc.techport.web.nailcashier.service.OrderService;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import com.google.gson.Gson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class OrderBusinessImpl implements OrderBusiness {

    public static final Log LOGGER = LogFactory.getLog(OrderBusinessImpl.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private GoodService goodService;

    private Gson gson = new Gson();

    @Override
    public Map<String, Object> createPurchaseOrder(PurchaseOrderInfo orderInfo) {
        try {
            if(null == orderInfo){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Invalid order info.").asMap();
            }

            Employee orderEmployee = getOrderEmployee(orderInfo);
            if(null == orderEmployee){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Must have a valid employee").asMap();
            }

            List<Good> orderGoods = getOrderGoods(orderInfo);
            if(null == orderGoods || orderGoods.isEmpty()){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Goods in order can not be empty.").asMap();
            }

            Member orderMember = getOrderMember(orderInfo.getMemberId());

            Double orderValue = 0d;
            Double cash = 0d;
            Double balance = 0d;
            Double revenue = 0d;

            for (Good good : orderGoods){
                orderValue += good.getPrice()*good.getStock();
            }

            //扣款逻辑
            //非会员
            if(null == orderMember){
                balance = 0d;
                cash = orderValue;
                revenue = cash;
            }else {
                //会员余额直接支付
                if (orderMember.getBalance() > orderValue * orderMember.getDiscount()/100) {
                    cash = 0d;
                    //保留两位小数
                    DecimalFormat df = new DecimalFormat("#.##");
                    balance = orderValue * orderMember.getDiscount() / 100 ;
                    balance = Double.parseDouble(df.format(balance));

                    revenue = balance + cash;
                    revenue = Double.parseDouble(df.format(revenue));
                } else {
                    //会员余额不足, 支付部分现金
                    balance = orderMember.getBalance();

                    DecimalFormat df = new DecimalFormat("#.##");
                    cash = orderValue - balance / orderMember.getDiscount() *100;
                    cash = Double.parseDouble(df.format(cash));

                    revenue = balance + cash;
                    revenue = Double.parseDouble(df.format(revenue));
                }
            }

            Order order = new Order();
            order.setOrderType(OrderType.PURCHASE);
            order.setOrderStatus(OrderStatus.VALID);
            if(null!=orderMember){
                order.setMemberId(orderMember.getMemberId()==null?-1 : orderMember.getMemberId());
                order.setMemberName(orderMember.getFullName());
                order.setMemberBalance(orderMember.getBalance());
                order.setMemberDiscount(orderMember.getDiscount());
            }
            order.setEmployeeIds("["+orderEmployee.getEmployeeId()+"]");
            order.setOrderInfo(gson.toJson(orderGoods));
            order.setOrderValue(orderValue);
            order.setBalance(balance);
            order.setCash(cash);
            order.setRevenue(revenue);
            order.setComments("");

            //订单信息持久化
            orderService.createOrder(order);
            for (Good good : orderGoods){
                Integer origin = goodService.getGoodById(good.getGoodId()).getStock();
                good.setStock(origin-good.getStock());
                goodService.setGoodStock(good);
            }

            //会员扣款
            if(orderMember!=null){
                memberService.chargeMember(orderMember.getMemberId(),balance);
            }

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Placed order!")
                    .property("order",order)
                    .asMap();

        } catch (Exception e) {
            LOGGER.error("Error placing order.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Error placing order.").asMap();
        }
    }

    @Override
    public Map<String, Object> createFundOrder(FundOrderInfo fundInfo) {
        try {
            if(null == fundInfo){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Invalid order info.").asMap();
            }
            
            Member orderMember = getOrderMember(fundInfo.getMemberId());
            if(null == orderMember){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Invalid member!").asMap();
            }

            List<Employee> employees = fundInfo.getEmployees();


            StringBuilder employeeIds = new StringBuilder();
            for (Employee employee : employees){
                employeeIds.append("[").append(employee.getEmployeeId()).append("]");
            }
//            if(!employeeIds.toString().isEmpty()&& employeeIds.charAt(employeeIds.length()-1) == '-'){
//                employeeIds.deleteCharAt(employeeIds.length()-1);
//            }

            DecimalFormat df = new DecimalFormat("#.##");

            Double cash = fundInfo.getCash();
            Double balance = fundInfo.getBalance();
            Double revenue = fundInfo.getCash();

            cash = Double.parseDouble(df.format(cash));
            balance = Double.parseDouble(df.format(balance));
            revenue = Double.parseDouble(df.format(revenue));

            Order order = new Order();
            order.setOrderType(OrderType.DEPOSIT);
            order.setOrderStatus(OrderStatus.VALID);
            order.setMemberId(orderMember.getMemberId());
            order.setMemberName(orderMember.getFullName());
            order.setMemberBalance(orderMember.getBalance());
            order.setOrderValue(balance);
            order.setCash(cash);
            order.setRevenue(revenue);
            order.setBalance(balance);
            order.setEmployeeIds(employeeIds.toString());
            order.setOrderInfo("[]");
            order.setComments("");

            //订单信息持久化
            orderService.createOrder(order);

            //会员扣款
            memberService.fundMember(orderMember.getMemberId(),balance);

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Placed order!")
                    .property("order",order)
                    .asMap();

        } catch (Exception e) {
            LOGGER.error("Error placing order.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Error placing order.").asMap();
        }
    }

    @Override
    public Map<String, Object> revokeOrder(Order order) {
        try {
            if(null == order){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Invalid order info.").asMap();
            }

            Order realOrder = orderService.getOrderById(order.getOrderId());

            if(realOrder.getOrderStatus().equals(OrderStatus.REVOKED)){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_STATE)
                        .respMsg("Already revoked!").asMap();
            }

            realOrder.setOrderStatus(OrderStatus.REVOKED);
            orderService.updateOrderStatus(realOrder);

            if (realOrder.getOrderType().equals(OrderType.PURCHASE)){
                if (null !=realOrder.getMemberId()){
                    memberService.fundMember(realOrder.getMemberId(), realOrder.getBalance());
                }
            }else if(realOrder.getOrderType().equals(OrderType.DEPOSIT)){
                memberService.chargeMember(realOrder.getMemberId(), realOrder.getBalance());
            }

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Revoked order!")
                    .property("order",order)
                    .asMap();

        } catch (Exception e) {
            LOGGER.error("Error revoking order.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Error revoking order.").asMap();
        }
    }

    private Employee getOrderEmployee(PurchaseOrderInfo orderInfo) {
        Integer employeeId = orderInfo.getEmployeeId();
        if (null == employeeId || employeeId<=0) {
            return null;
        }
        Employee employee = employeeService.getEmployeeById(employeeId);
        if(null == employee){
            throw new IllegalStateException("Invalid employeeId!!!");
        }
        return employee;
    }

    private Member getOrderMember(Integer memberId){
        if (null == memberId || memberId<=0) {
            return null;
        }
        Member orderMember = memberService.getMemberById(memberId);

        if(null== orderMember){
            throw new IllegalStateException("Invalid memberId!!!");
        }
        return orderMember;
    }

    private List<Good> getOrderGoods(PurchaseOrderInfo orderInfo){
        List<Good> orderGoods = new ArrayList<>();
        for (Good good : orderInfo.getOrderGoods()){
            Good thisGood = goodService.getGoodById(good.getGoodId());
            thisGood.setStock(good.getStock());
            orderGoods.add(thisGood);
        }
        return orderGoods;
    }

    @Override
    public Map<String, Object> updateOrderStatus(Order order) {
        try{
            if(order.getOrderId()==null){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Must have an orderId.").asMap();
            }

            orderService.updateOrderStatus(order);

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Updated order Status").asMap();

        }catch (Exception e){
            LOGGER.error("Error updating order.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Error updating order.").asMap();
        }
    }

    @Override
    public Map<String, Object> updateOrderComments(Order order) {
        try{
            if(order.getOrderId()==null){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Must have an orderId.").asMap();
            }

            orderService.updateOrderComment(order);

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Updated order comments.").asMap();

        }catch (Exception e){
            LOGGER.error("Error updating order.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Error updating order.").asMap();
        }
    }

    @Override
    public Map<String, Object> getBusinessReport(String startTime, String endTime) {
        return null;
    }

    @Override
    public Map<String, Object> getOrderByTime(String startTime, String endTime) {
        try{
            if(startTime==null || endTime == null){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Must have valid start & end time.").asMap();
            }

            List<Order> orders = orderService.getOrderByTime(startTime,endTime);

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Fetched order comments.")
                    .property("orders",orders)
                    .asMap();

        }catch (Exception e){
            LOGGER.error("Error getting order.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Error getting order.").asMap();
        }
    }

    @Override
    public Map<String, Object> getOrderByMember(String startTime, String endTime, Integer memberId) {
        try{
            if(startTime==null || endTime == null || memberId==null){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Must have valid start & end time & memberId.").asMap();
            }

            List<Order> orders = orderService.getOrderByMember(startTime,endTime,memberId);

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Fetched order comments.")
                    .property("orders",orders)
                    .asMap();

        }catch (Exception e){
            LOGGER.error("Error getting order.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Error getting order.").asMap();
        }
    }

    @Override
    public Map<String, Object> queryOrder(QueryOrderInfo queryInfo) {
        try{
            if(queryInfo==null){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Must have valid queryInfo.").asMap();
            }

            if(queryInfo.getStartTime()==null || queryInfo.getEndTime()==null){
                return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Must have valid start & end time.").asMap();
            }

            List<Order> orders = orderService.queryOrder(
                    new DateTime(queryInfo.getStartTime()).toString("yyyy-MM-dd 00:00:00") ,
                    new DateTime(queryInfo.getEndTime()).toString("yyyy-MM-dd 23:59:59") ,
                    queryInfo.getOrderType(),
                    queryInfo.getMemberId(),
                    queryInfo.getEmployeeId()
            );

            return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Fetched order comments.")
                    .property("orders",orders)
                    .asMap();

        }catch (Exception e){
            LOGGER.error("Error getting order.", e);
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Error getting order.").asMap();
        }
    }
}
