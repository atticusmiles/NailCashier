package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.controller.LoginController;

import java.util.Map;

public interface LoginBusiness {

    Map<String, Object> doLogin(LoginController.LoginForm loginForm);

    Map<String, Object> doLogout();

    Map<String,Object> getUserInfo(String authKey);
}
