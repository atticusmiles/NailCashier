package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.model.Good;
import cc.techport.web.nailcashier.service.GoodService;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class GoodBusinessImpl implements GoodBusiness {
    public static final Log LOGGER = LogFactory.getLog(GoodBusinessImpl.class);

    @Autowired
    private GoodService goodService;

    @Override
    public Map<String, Object> createGood(Good good) {
        try {
            if(null == good){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Good can't be null!").asMap();
            }

            if(StringUtils.isBlank(good.getGoodName())){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Good must have a name !").asMap();
            }

            goodService.createGood(good);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Create good done.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception creating good!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception creating good!").asMap();
        }
    }

    @Override
    public Map<String, Object> updateGoodInfo(Good good) {
        try {
            if(null == good){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Good can't be null!").asMap();
            }

            if(null == good.getGoodId() || 0 == good.getGoodId()){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Good must have an id!").asMap();
            }

            goodService.updateGoodInfo(good);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Updated good info.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception updating good!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception updating good!").asMap();
        }
    }

    @Override
    public Map<String, Object> deleteGood(Good good) {
        try {
            if(null == good){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Good can't be null!").asMap();
            }

            if(null == good.getGoodId() || 0 == good.getGoodId()){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Good must have an id!").asMap();
            }

            goodService.deleteGood(good);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Deleted good.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception deleting good!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception deleting good!").asMap();
        }
    }

    @Override
    public Map<String, Object> getAllGoods() {
        try {
            List<Good> goods = goodService.getAllGoods();
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Getting goods done.")
                    .property("goods",goods)
                    .asMap();
        } catch (Exception e){
            LOGGER.error("Unknown exception getting all goods!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception getting all goods!").asMap();
        }
    }

    @Override
    public Map<String, Object> setGoodStock(Good good) {
        try {
            if(null == good){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Good can't be null!").asMap();
            }

            if(null == good.getGoodId() || 0 == good.getGoodId()){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Good must have an id!").asMap();
            }

            goodService.setGoodStock(good);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Increased good stock.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception increasing good stock!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception increasing good stock!").asMap();
        }
    }

}
