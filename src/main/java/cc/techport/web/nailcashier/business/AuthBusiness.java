package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.model.AuthGroup;
import cc.techport.web.nailcashier.model.User;

import java.util.Map;

public interface AuthBusiness {

    //For controllers
    Map<String,Object> createAuthGroup(AuthGroup authGroup);
    Map<String,Object> deleteAuthGroup(AuthGroup authGroup);
    Map<String,Object> updateAuthGroup(AuthGroup authGroup);
    Map<String,Object> getAllAuthGroups();

    //for internal use
    boolean isPathFree(String path);
    String issueKeyForUser(User user);
    boolean isPathAuthedForUser(String path, User user);
}
