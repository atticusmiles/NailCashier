package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.model.User;

import java.util.Map;

public interface UserBusiness {

    Map<String,Object> createUser(User user);

    Map<String,Object> updateUserInfo(User user);

    Map<String,Object> updateUserPwd(User user);

    Map<String,Object> deleteUser(User user);

    Map<String,Object> getAllUsers();

}
