package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.model.Member;
import cc.techport.web.nailcashier.service.MemberService;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class MemberBusinessImpl implements MemberBusiness {

    public static final Log LOGGER = LogFactory.getLog(MemberBusinessImpl.class);

    @Autowired
    private MemberService memberService;

    @Override
    public Map<String, Object> createMember(Member member) {
        try {
            LOGGER.info("Creating member: " +member );

            if(null == member){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Member can't be null!").asMap();
            }

            if(!member.isValid()){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Member must have a name and a mobile!").asMap();
            }

            memberService.createMember(member);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Create member done.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception creating member!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception creating member!").asMap();
        }
    }

    @Override
    public Map<String, Object> updateMemberInfo(Member member) {
        try {
            if(null == member){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Member can't be null!").asMap();
            }

            if(!member.isValid()){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Member must have a name and a mobile!").asMap();
            }

            memberService.updateMemberInfo(member);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Update member done.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception updating member info!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception updating member info!").asMap();
        }
    }

    @Override
    public Map<String, Object> deleteMember(Member member) {
        try {
            if(null == member){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Member can't be null!").asMap();
            }

            if(null ==member.getMemberId()){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Member must have an id!").asMap();
            }

            memberService.deleteMember(member);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Delete member done.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception deleting member!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception deleting member!").asMap();
        }
    }

    @Override
    public Map<String, Object> getAllMembers() {
        try {
            List<Member> members = memberService.getAllMembers();
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Getting member done.")
                    .property("members",members)
                    .asMap();
        } catch (Exception e){
            LOGGER.error("Unknown exception getting all members!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception getting all member!").asMap();
        }
    }

    @Override
    public Map<String, Object> getMemberById(Integer memberId) {
        try {
            if (null == memberId){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("MemberId can't be null!").asMap();
            }
            Member member = memberService.getMemberById(memberId);
            if(null == member){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_STATE)
                        .respMsg("Can't find such member!").asMap();
            }
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Getting member done.")
                    .property("member",member)
                    .asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception getting member!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception getting member!").asMap();
        }
    }

    @Override
    public Map<String, Object> getMemberByFullName(String fullName) {
        try {
            if (null == fullName){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("FullName can't be null!").asMap();
            }

            List<Member> members = memberService.getMemberByFullName(fullName);

            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Getting member done.")
                    .property("members",members)
                    .asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception getting member!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception getting member!").asMap();
        }
    }

    @Override
    public Map<String, Object> getMemberByMobile(String mobile) {
        try {

            if (null == mobile){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Mobile can't be null!").asMap();
            }

            List<Member> members = memberService.getMemberByMobile(mobile);

            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Getting member done.")
                    .property("members",members)
                    .asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception getting member!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception getting member!").asMap();
        }
    }

    @Override
    public Map<String, Object> fundMember(Member member) {
        try {

            if (null == member.getMemberId()){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("memberId can't be null!").asMap();
            }

           memberService.fundMember(member.getMemberId(),member.getBalance());

            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Fund member done.")
                    .asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception funding member!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception funding member!").asMap();
        }
    }
}
