package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.model.Employee;
import cc.techport.web.nailcashier.service.EmployeeService;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class EmployeeBusinessImpl implements EmployeeBusiness {

    public static final Log LOGGER = LogFactory.getLog(EmployeeBusinessImpl.class);

    @Autowired
    private EmployeeService employeeService;

    @Override
    public Map<String, Object> createEmployee(Employee employee) {
        try {
            if(null == employee){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Employee can't be null!").asMap();
            }

            if(StringUtils.isBlank(employee.getFullName())){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Employee must have a name !").asMap();
            }

            employeeService.createEmployee(employee);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Create employee done.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception creating employee!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception creating employee!").asMap();
        }
    }

    @Override
    public Map<String, Object> updateEmployee(Employee employee) {
        try {
            if(null == employee){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Employee can't be null!").asMap();
            }

            employeeService.updateEmployee(employee);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Update employee done.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception updating employee!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception updating employee!").asMap();
        }
    }

    @Override
    public Map<String, Object> deleteEmployee(Employee employee) {
        try {
            if(null == employee){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Employee can't be null!").asMap();
            }

            if( null == employee.getEmployeeId() || 0 == employee.getEmployeeId() ){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Employee must have an Id !").asMap();
            }

            employeeService.deleteEmployee(employee);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .respMsg("Delete employee done.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception deleting employee!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception deleting employee!").asMap();
        }
    }

    @Override
    public Map<String, Object> getAllEmployees() {
        try {
            List<Employee> employees = employeeService.getAllEmployee();
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .property("employees", employees)
                    .respMsg("Getting employees done.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception getting employee!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception getting employee!").asMap();
        }
    }

    @Override
    public Map<String, Object> getEmployeeById(Employee employee) {
        try {
            if(null == employee){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Employee can't be null!").asMap();
            }

            if( null == employee.getEmployeeId() || 0 == employee.getEmployeeId() ){
                return ResponseBuilder.builder()
                        .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                        .respMsg("Employee must have an Id !").asMap();
            }

            Employee employeeById = employeeService.getEmployeeById(employee.getEmployeeId());
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_SUCCESS)
                    .property("employee", employeeById)
                    .respMsg("Getting employee done.").asMap();

        } catch (Exception e){
            LOGGER.error("Unknown exception getting employee!",e);
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_UNKOWN_EXCEPTION)
                    .respMsg("Unknown exception getting employee!").asMap();
        }
    }
}
