package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.model.AuthGroup;
import cc.techport.web.nailcashier.model.Member;

import java.util.Map;

public interface MemberBusiness {

    Map<String,Object> createMember(Member member);

    Map<String,Object> updateMemberInfo(Member member);

    Map<String,Object> deleteMember(Member member);

    Map<String,Object> getAllMembers();

    Map<String,Object> getMemberById(Integer memberId);

    Map<String,Object> getMemberByFullName(String fullName);

    Map<String,Object> getMemberByMobile(String mobile);

    Map<String,Object> fundMember(Member member);

}
