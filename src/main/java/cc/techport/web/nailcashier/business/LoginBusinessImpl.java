package cc.techport.web.nailcashier.business;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.controller.LoginController;
import cc.techport.web.nailcashier.model.User;
import cc.techport.web.nailcashier.service.AuthGroupService;
import cc.techport.web.nailcashier.service.JWTService;
import cc.techport.web.nailcashier.service.UserService;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import cc.techport.web.nailcashier.utils.SecurityUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class LoginBusinessImpl implements LoginBusiness{

    @Autowired
    private UserService userService;
    @Autowired
    private AuthBusiness authBusiness;
    @Autowired
    private JWTService jwtService;
    @Autowired
    private AuthGroupService authGroupService;


    @Override
    public Map<String, Object> doLogin(LoginController.LoginForm loginForm) {

        if(StringUtils.isBlank(loginForm.getUsername())||StringUtils.isBlank(loginForm.getPassword())){
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_ILLEGAL_ARGUMENT)
                    .respMsg("Username and password can't be blank.").asMap();
        }

        String encryptedPwd = SecurityUtil.makeMD5(loginForm.getPassword());
        String username = loginForm.getUsername();
        if(!userService.isPwdValid(username,encryptedPwd)){
            return ResponseBuilder.builder()
                    .respCode(Constants.RESPCODE_ERROR_ILLEGAL_STATE)
                    .respMsg("Password is not correct.").asMap();
        }

        User realUser = userService.getUserByUsername(username);
        String authKey = authBusiness.issueKeyForUser(realUser);

        return ResponseBuilder.builder()
                .respCode(Constants.RESPCODE_SUCCESS)
                .respMsg("Login success, welcome!")
                .property("username",realUser.getUsername())
                .property("userId",realUser.getUserId())
                .property("redirect","/")
                .property("authKey",authKey).asMap();
    }

    @Override
    public Map<String, Object> doLogout() {
        return ResponseBuilder.builder()
                .respCode(Constants.RESPCODE_SUCCESS)
                .respMsg("Logout success, bye!")
                .property("redirect","login")
                .property("authKey", "").asMap();
    }

    @Override
    public Map<String, Object> getUserInfo(String authKey) {
        if(!jwtService.isAuthKeyValid(authKey)){
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNAUTORIZED_ACCESS)
                    .respMsg("Authkey has expired, please login.")
                    .property("redirect","login").asMap();
        }

        JWTService.AuthForm authForm = jwtService.decodeAuthKey(authKey);
        User realUser = userService.getUserByUsername(authForm.getUsername());
        if(null == realUser){
            return ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_ILLEGAL_STATE)
                    .respMsg("Invalid user!")
                    .property("redirect","login").asMap();
        }

        String modString = authGroupService.getAuthGroupByName(realUser.getAuthGroup()).getFrontendMods();
        List<String> modList = Arrays.asList(modString.split(","));

        Map<String,Object> userInfo = new HashMap<>();
        userInfo.put("username",realUser.getUsername());
        userInfo.put("userId",realUser.getUserId());
        userInfo.put("authedMods",modList);

        return ResponseBuilder.builder().respCode(Constants.RESPCODE_SUCCESS)
                .respMsg("Done fetching user info.")
                .property("userInfo",userInfo).asMap();
    }

}
