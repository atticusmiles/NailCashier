package cc.techport.web.nailcashier;

public class Constants {

    public static final String RESPCODE_SUCCESS = "1000";

    public static final String RESPCODE_ERROR_UNAUTORIZED_ACCESS = "4001";
    public static final String RESPCODE_ERROR_ILLEGAL_ARGUMENT = "4002";
    public static final String RESPCODE_ERROR_ILLEGAL_STATE = "4003";

    public static final String RESPCODE_ERROR_UNKOWN_EXCEPTION = "5000";

    public static final String AUTH_HMCA256_KEY = "nailCashier.1qaz@WSX";
    public static final String ANONYMOUS_USERNAME = "anonymous";
    public static final String ADMIN_USERNAME = "admin";

    public static final String AUTHKEY_COOKIE_NAME = "AuthKey";
    public static final Integer AUTHKEY_VALID_HOURS = 2;
    public static final Integer AUTHKEY_LEEWAY_HOURS = 24;

    public static final Integer AUTHKEY_RENEWAL_MINUTES = 120;
}
