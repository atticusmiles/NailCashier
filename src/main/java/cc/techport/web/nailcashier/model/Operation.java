package cc.techport.web.nailcashier.model;

import cc.techport.web.nailcashier.dto.OperationType;

public class Operation {
    private Integer operationId;
    private Integer memberId;
    private Integer employeeId;
    private Integer operationInfo;
    private OperationType operationType;
    private String createAt;

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getOperationInfo() {
        return operationInfo;
    }

    public void setOperationInfo(Integer operationInfo) {
        this.operationInfo = operationInfo;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }
}
