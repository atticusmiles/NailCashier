package cc.techport.web.nailcashier.model;

import org.apache.commons.lang3.StringUtils;

public class Employee {

    private Integer employeeId;

    private String fullName = "";

    private String mobile = "";

    private Double serviceCut = 0d;

    private Double depositCut = 0d;

    private String comments = "";

    private String createAt;

    private String updateAt;

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }

    public Double getServiceCut() {
        return serviceCut;
    }

    public void setServiceCut(Double serviceCut) {
        this.serviceCut = serviceCut;
    }

    public Double getDepositCut() {
        return depositCut;
    }

    public void setDepositCut(Double depositCut) {
        this.depositCut = depositCut;
    }

    public boolean isValid(){
        return StringUtils.isNotBlank(fullName) && StringUtils.isNotBlank(mobile);
    }
}
