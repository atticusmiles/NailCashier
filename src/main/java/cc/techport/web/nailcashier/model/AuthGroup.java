package cc.techport.web.nailcashier.model;

import org.joda.time.DateTime;

public class AuthGroup {
    private Integer groupId;
    private String groupName;
    private String frontendMods;
    private String backendPaths;
    private String createAt;
    private String updateAt;

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFrontendMods() {
        return frontendMods;
    }

    public void setFrontendMods(String frontendMods) {
        this.frontendMods = frontendMods;
    }

    public String getBackendPaths() {
        return backendPaths;
    }

    public void setBackendPaths(String backendPaths) {
        this.backendPaths = backendPaths;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(String updateAt) {
        this.updateAt = updateAt;
    }
}
