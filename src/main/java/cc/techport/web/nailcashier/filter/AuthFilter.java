package cc.techport.web.nailcashier.filter;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.business.AuthBusiness;
import cc.techport.web.nailcashier.model.User;
import cc.techport.web.nailcashier.service.JWTService;
import cc.techport.web.nailcashier.service.UserService;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component(value = "authFilter")
public class AuthFilter extends OncePerRequestFilter {

    public static final Log LOG = LogFactory.getLog(AuthFilter.class);

    @Autowired
    private JWTService jwtService;
    @Autowired
    private AuthBusiness authBusiness;
    @Autowired
    private UserService userService;

    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
//        //CORS Filter
//        if("OPTIONS".equals(httpServletRequest.getMethod().toUpperCase())){
//            filterChain.doFilter(httpServletRequest,httpServletResponse);
//            return;
//        }

        String path = httpServletRequest.getRequestURI();
        if (authBusiness.isPathFree(path)) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        String authKey = getAuthKey(httpServletRequest);
        boolean isAuthKeyValid = jwtService.isAuthKeyValid(authKey);
        if (isAuthKeyValid) {
            JWTService.AuthForm authForm = jwtService.decodeAuthKey(authKey);

            //if authkey is valid but expiring, reissue a new authkey
            if (jwtService.isAuthKeyExpiring(authKey)) {
                User user = userService.getUserByUsername(authForm.getUsername());
                String newAuthKey = authBusiness.issueKeyForUser(user);
                Cookie cookie = new Cookie(Constants.AUTHKEY_COOKIE_NAME, newAuthKey);
                cookie.setHttpOnly(true);
                cookie.setPath("/");
                cookie.setMaxAge(-1);
                httpServletResponse.addCookie(cookie);
            }

            if (isPathAuthed(path, authForm.getAuthedPaths())) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            } else {
                httpServletResponse.setStatus(200);
                try(PrintWriter writer = httpServletResponse.getWriter()){
                    writer.print(ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNAUTORIZED_ACCESS)
                            .respMsg("Unauthorized access!").asJson());
                    writer.flush();
                }
            }
        } else {
            User user = userService.getUserByUsername(Constants.ANONYMOUS_USERNAME);
            if (authBusiness.isPathAuthedForUser(path, user)) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            } else {
                httpServletResponse.setStatus(200);
                try (PrintWriter writer = httpServletResponse.getWriter()) {
                    writer.print(ResponseBuilder.builder().respCode(Constants.RESPCODE_ERROR_UNAUTORIZED_ACCESS)
                            .respMsg("Unauthorized access!").property("redirect", "login").asJson());
                    writer.flush();
                } catch (IOException e) {
                    LOG.error("Error writing to http response.", e);
                }
            }
        }
    }

    private String getAuthKey(HttpServletRequest request) {

        List<Cookie> cookies = request.getCookies() == null ? new ArrayList<Cookie>() : Arrays.asList(request.getCookies());
        String authKey = null;
        for (Cookie cookie : cookies) {
            if ("AuthKey".equals(cookie.getName())) {
                authKey = cookie.getValue();
                break;
            }
        }
        return authKey;
    }

    private boolean isPathAuthed(String pathToVerify, String authedPathString) {
        if (StringUtils.isBlank(pathToVerify) || StringUtils.isBlank(authedPathString)) {
            return false;
        }
        List<String> authedPaths = Arrays.asList(authedPathString.split(","));
        for (String authedPath : authedPaths) {
            if (pathToVerify.startsWith(authedPath)) {
                return true;
            }
        }
        return false;
    }
}