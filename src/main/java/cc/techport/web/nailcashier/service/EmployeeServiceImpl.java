package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.dao.EmployeeDao;
import cc.techport.web.nailcashier.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired(required = false)
    private EmployeeDao employeeDao;

    @Override
    public void createEmployee(Employee employee) {
        employeeDao.createEmployee(employee);
    }

    @Override
    public void updateEmployee(Employee employee) {
        employeeDao.updateEmployee(employee);
    }

    @Override
    public void deleteEmployee(Employee employee) {
        employeeDao.deleteEmployee(employee);
    }

    @Override
    public List<Employee> getAllEmployee() {
        List<Employee> employees = employeeDao.getAllEmployees();
        return employees ==null ? new ArrayList<Employee>() : employees;
    }

    @Override
    public Employee getEmployeeById(Integer employeeId) {
        List<Employee> employees = employeeDao.getEmployeeById(employeeId);
        return (employees ==null || employees.isEmpty()) ? null : employees.get(0);
    }
}
