package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.model.Good;

import java.util.List;

public interface GoodService {

    void createGood(Good good);

    void updateGoodInfo(Good good);

    void deleteGood(Good good);

    List<Good> getAllGoods();

    void setGoodStock(Good good);

    Good getGoodById(Integer goodId);

}
