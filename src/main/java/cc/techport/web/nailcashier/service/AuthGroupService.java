package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.model.AuthGroup;

import java.util.List;


public interface AuthGroupService {

    void createAuthGroup(AuthGroup authGroup);

    void deleteAuthGroup(AuthGroup authGroup);

    void updateAuthGroup(AuthGroup authGroup);

    AuthGroup getAuthGroupByName(String groupName);

    AuthGroup getAuthGroupById(Integer groupId);

    List<AuthGroup> getAllAuthGroups();

}
