package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.model.Employee;

import java.util.List;

public interface EmployeeService {

    void createEmployee(Employee employee);

    void updateEmployee(Employee employee);

    void deleteEmployee(Employee employee);

    List<Employee> getAllEmployee();

    Employee getEmployeeById(Integer employeeId);
}
