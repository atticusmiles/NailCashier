package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.model.User;

import java.util.List;
import java.util.Map;

public interface UserService {

    void createUser(User user);

    void updateUserInfo(User user);

    void updateUserPwd(User user);

    void deleteUser(User user);

    List<User> getAllUsers();

    User getUserByUsername(String username);

    User getUserByUserId(Integer userId);

    boolean isPwdValid(String username, String encryptedPwd);
}
