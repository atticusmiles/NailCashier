package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.model.Member;

import java.util.List;

public interface MemberService {

    void createMember(Member member);

    void updateMemberInfo(Member member);

    void deleteMember(Member member);

    List<Member> getAllMembers();

    Member getMemberById(Integer memberId);

    List<Member> getMemberByFullName(String fullName);

    List<Member> getMemberByMobile(String mobile);

    void fundMember(Integer memberId, double amount);

    void chargeMember(Integer memberId, double amount);
}
