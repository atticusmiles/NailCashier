package cc.techport.web.nailcashier.service;

public interface ConfigService {

    void createConfig(String key, String value);

    void deleteConfig(String key);

    void updateConfig(String key, String value);

    String getConfig(String key);
}
