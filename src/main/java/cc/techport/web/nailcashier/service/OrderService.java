package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.dto.OrderType;
import cc.techport.web.nailcashier.model.Order;

import java.util.List;

public interface OrderService {

    void createOrder(Order order);

    void updateOrderStatus(Order order);

    void updateOrderComment(Order order);

    Double getRevenue(String startTime, String endTime, OrderType orderType);

    Double getCash(String startTime, String endTime, OrderType orderType);

    Double getBalance(String startTime, String endTime, OrderType orderType);

    Order getOrderById(Integer orderId);

    List<Order> getOrderByTime(String startTime, String endTime);

    List<Order> getOrderByMember(String startTime, String endTime, Integer memberId);

    List<Order> queryOrder(String startDate, String endDate, String orderType, Integer memberId, Integer employeeId);

}
