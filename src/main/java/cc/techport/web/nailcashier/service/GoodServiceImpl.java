package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.dao.GoodDao;
import cc.techport.web.nailcashier.model.Good;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GoodServiceImpl implements GoodService {
    @Autowired(required = false)
    private GoodDao goodDao;

    @Override
    public void createGood(Good good) {
        goodDao.createGood(good);
    }

    @Override
    public void updateGoodInfo(Good good) {
        goodDao.updateGoodInfo(good);
    }

    @Override
    public void deleteGood(Good good) {
        goodDao.deleteGood(good);
    }

    @Override
    public List<Good> getAllGoods() {
        List<Good> goods = goodDao.getAllGoods();
        return goods ==null ? new ArrayList<Good>() : goods;
    }

    @Override
    public void setGoodStock(Good good) {
        goodDao.setGoodStock(good);
    }

    @Override
    public Good getGoodById(Integer goodId) {
        List<Good> goods = goodDao.getGoodById(goodId);
        if(goods==null || goods.isEmpty()){
            return null;
        }else {
            return goods.get(0);
        }
    }

}
