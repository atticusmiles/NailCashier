package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.dao.OrderDao;
import cc.techport.web.nailcashier.dto.OrderType;
import cc.techport.web.nailcashier.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements
        OrderService {

    @Autowired(required = false)
    private OrderDao orderDao;

    @Override
    public void createOrder(Order order) {
        orderDao.createOrder(order);
    }

    @Override
    public void updateOrderStatus(Order order) {
        orderDao.updateOrderStatus(order);
    }

    @Override
    public void updateOrderComment(Order order) {
        orderDao.updateOrderComment(order);
    }

    @Override
    public Double getRevenue(String startTime, String endTime, OrderType orderType) {
        return orderDao.getRevenue(startTime,endTime,orderType.name());
    }

    @Override
    public Double getCash(String startTime, String endTime, OrderType orderType) {
        return orderDao.getCash(startTime,endTime,orderType.name());
    }

    @Override
    public Double getBalance(String startTime, String endTime, OrderType orderType) {
        return orderDao.getBalance(startTime,endTime,orderType.name());
    }

    @Override
    public Order getOrderById(Integer orderId) {
        List<Order> orders = orderDao.getOrderById(orderId);
        return orders == null ? null : orders.get(0);
    }

    @Override
    public List<Order> getOrderByTime(String startTime, String endTime) {
        List<Order> orders = orderDao.getOrderByTime(startTime,endTime);
        return orders == null ? new ArrayList<Order>() : orders;
    }

    @Override
    public List<Order> getOrderByMember(String startTime, String endTime, Integer memberId) {
        List<Order> orders = orderDao.getOrderByMember(startTime,endTime,memberId);
        return orders == null ? new ArrayList<Order>() : orders;
    }

    @Override
    public List<Order> queryOrder(String startDate, String endDate, String orderType, Integer memberId, Integer employeeId) {
        List<Order> orders = orderDao.queryOrder(startDate,endDate,orderType,memberId,employeeId);
        return orders == null ? new ArrayList<Order>() : orders;
    }
}
