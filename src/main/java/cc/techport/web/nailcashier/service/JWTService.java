package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.model.User;
import com.auth0.jwt.exceptions.JWTVerificationException;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

public interface JWTService {

    final class AuthForm {
        private String username;
        private Integer userId;
        private String authedPaths;
        private DateTime issuedAt;
        private DateTime expiredAt;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getAuthedPaths() {
            return authedPaths;
        }

        public void setAuthedPaths(String authedPaths) {
            this.authedPaths = authedPaths;
        }

        public DateTime getIssuedAt() {
            return issuedAt;
        }

        public void setIssuedAt(DateTime issuedAt) {
            this.issuedAt = issuedAt;
        }

        public DateTime getExpiredAt() {
            return expiredAt;
        }

        public void setExpiredAt(DateTime expiredAt) {
            this.expiredAt = expiredAt;
        }

        public boolean isValid() {
            return StringUtils.isNotBlank(username) && null != userId && null != authedPaths;
        }

    }

    String genAuthKey(AuthForm form);

    AuthForm decodeAuthKey(String authKey) throws JWTVerificationException;

    boolean isAuthKeyExpiring(String authKey) throws JWTVerificationException;

    boolean isAuthKeyValid(String authKey);
}
