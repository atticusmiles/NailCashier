package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.dao.AuthGroupDao;
import cc.techport.web.nailcashier.model.AuthGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthGroupServiceImpl implements AuthGroupService {

    @Autowired(required = false)
    private AuthGroupDao authGroupDao;

    @Override
    public void createAuthGroup(AuthGroup authGroup) {
        authGroupDao.createAuthGroup(authGroup);
    }

    @Override
    public void deleteAuthGroup(AuthGroup authGroup) {
        authGroupDao.deleteAuthGroup(authGroup);
    }

    @Override
    public void updateAuthGroup(AuthGroup authGroup) {
        authGroupDao.updateAuthGroup(authGroup);
    }

    @Override
    public List<AuthGroup> getAllAuthGroups() {
        List<AuthGroup> allGroups = authGroupDao.getAllAuthGroups();
        return allGroups == null? new ArrayList<AuthGroup>() : allGroups;
    }

    @Override
    public AuthGroup getAuthGroupByName(String groupName) {
        List<AuthGroup> groups = authGroupDao.getAuthGroupByName(groupName);
        return groups == null ? null : groups.get(0);
    }

    @Override
    public AuthGroup getAuthGroupById(Integer groupId) {
        List<AuthGroup> groups = authGroupDao.getAuthGroupByGroupId(groupId);
        return groups == null ? null : groups.get(0);
    }

}
