package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.dao.MemberDao;
import cc.techport.web.nailcashier.model.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MemberServiceImpl implements MemberService{

    @Autowired(required = false)
    private MemberDao memberDao;

    @Override
    public void createMember(Member member) {
        memberDao.createMember(member);
    }

    @Override
    public void updateMemberInfo(Member member) {
        memberDao.updateMemberInfo(member);
    }

    @Override
    public void deleteMember(Member member) {
        memberDao.deleteMember(member);
    }

    @Override
    public List<Member> getAllMembers() {
        List<Member> allMembers = memberDao.getAllMembers();
        return null == allMembers ? new ArrayList<Member>() : allMembers;
    }

    @Override
    public Member getMemberById(Integer memberId) {
        List<Member> selected = memberDao.getMemberById(memberId);
        return (null != selected && !selected.isEmpty()) ? selected.get(0) : null;
    }

    @Override
    public List<Member> getMemberByFullName(String fullName) {
        List<Member> selected = memberDao.getMemberByFullName(fullName);
        return null == selected ? new ArrayList<Member>() : selected;
    }

    @Override
    public List<Member> getMemberByMobile(String mobile) {
        List<Member> selected = memberDao.getMemberByMobile(mobile);
        return null == selected ? new ArrayList<Member>() : selected;
    }

    @Override
    public void fundMember(Integer memberId, double amount) {
        memberDao.fundMember(memberId, amount);
    }

    @Override
    public void chargeMember(Integer memberId, double amount) {
        memberDao.chargeMember(memberId, amount);
    }
}
