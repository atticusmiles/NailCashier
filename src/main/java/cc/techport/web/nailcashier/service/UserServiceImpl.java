package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.dao.UserDao;
import cc.techport.web.nailcashier.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired(required = false)
    private UserDao userDao;

    @Override
    public void createUser(User user) {
        userDao.createUser(user);
    }

    @Override
    public void updateUserInfo(User user) {
        userDao.updateUserInfo(user);
    }

    @Override
    public void updateUserPwd(User user) {
        userDao.updateUserPwd(user);
    }

    @Override
    public void deleteUser(User user) {
        userDao.deleteUser(user);
    }

    @Override
    public List<User> getAllUsers() {
        List<User> allUsers = userDao.getAllUsers();
        return  null == allUsers ? new ArrayList<User>() : allUsers;
    }

    @Override
    public User getUserByUsername(String username) {
        List<User> selected = userDao.getUserByUsername(username);
        if(null == selected || selected.isEmpty()){
            return null;
        } else {
            return selected.get(0);
        }
    }

    @Override
    public User getUserByUserId(Integer userId) {
        List<User> selected = userDao.getUserByUserId(userId);
        if(null == selected || selected.isEmpty()){
            return null;
        } else {
            return selected.get(0);
        }
    }

    @Override
    public boolean isPwdValid(String username, String encryptedPwd) {
        Integer count = userDao.isPwdAMatch(username, encryptedPwd);
        return 1 == count;
    }

}
