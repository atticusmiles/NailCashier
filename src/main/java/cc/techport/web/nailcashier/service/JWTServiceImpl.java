package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.Constants;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class JWTServiceImpl implements InitializingBean, JWTService {

    private Algorithm algorithm;
    private JWTVerifier verifier;

    public void afterPropertiesSet() throws Exception {
        algorithm = Algorithm.HMAC256(Constants.AUTH_HMCA256_KEY);
        verifier = JWT.require(algorithm)
                .acceptLeeway(Constants.AUTHKEY_LEEWAY_HOURS*3600)
                .withIssuer("nailCashier.techport.cc")
                .build();
    }

    @Override
    public String genAuthKey(JWTService.AuthForm form) {
        if (!form.isValid()) {
            throw new IllegalArgumentException("Form is not valid");
        }

        JWTCreator.Builder builder = JWT.create()
                .withIssuer("nailCashier.techport.cc")
                .withIssuedAt(new DateTime().toDate())
                .withExpiresAt(new DateTime().plusHours(Constants.AUTHKEY_VALID_HOURS).toDate());

        builder.withClaim("userId", form.getUserId())
                .withClaim("username", form.getUsername())
                .withClaim("authedPaths", form.getAuthedPaths());

        return builder.sign(algorithm);
    }

    @Override
    public JWTService.AuthForm decodeAuthKey(String authKey) throws JWTVerificationException {
        JWTService.AuthForm authForm = new JWTService.AuthForm();
        DecodedJWT jwt = verifier.verify(authKey);

        authForm.setUserId(jwt.getClaim("userId").asInt());
        authForm.setUsername(jwt.getClaim("username").asString());
        authForm.setAuthedPaths(jwt.getClaim("authedPaths").asString());
        authForm.setIssuedAt(new DateTime(jwt.getIssuedAt()));
        authForm.setExpiredAt(new DateTime(jwt.getExpiresAt()));

        return authForm;
    }


    @Override
    public boolean isAuthKeyExpiring(String authKey) throws JWTVerificationException{
        if (StringUtils.isBlank(authKey)) {
            return false;
        }
        DecodedJWT jwt = verifier.verify(authKey);
        DateTime expiredAt = new DateTime(jwt.getExpiresAt());
        return expiredAt.isBeforeNow();
    }

    @Override
    public boolean isAuthKeyValid(String authKey) {
        try {
            if(StringUtils.isBlank(authKey)){
                return false;
            }
            verifier.verify(authKey);
            return true;
        }catch (JWTVerificationException e){
            return false;
        }
    }
}
