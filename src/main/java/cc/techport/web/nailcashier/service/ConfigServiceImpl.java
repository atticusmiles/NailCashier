package cc.techport.web.nailcashier.service;

import cc.techport.web.nailcashier.dao.ConfigDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConfigServiceImpl implements ConfigService {

    @Autowired(required = false)
    private ConfigDao configDao;

    @Override
    public void createConfig(String key, String value) {
        if(StringUtils.isNotBlank(key) && null!=value){
            configDao.createConfig(key,value);
        }
    }

    @Override
    public void deleteConfig(String key) {
        if(StringUtils.isNotBlank(key)) {
            configDao.deleteConfig(key);
        }
    }

    @Override
    public void updateConfig(String key, String value) {
        if(StringUtils.isNotBlank(key) && null!=value){
            configDao.updateConfig(key, value);
        }
    }

    @Override
    public String getConfig(String key) {
        if(StringUtils.isBlank(key)){
            return null;
        }

        List<String> configs = configDao.getConfig(key);

        if(null == configs || configs.isEmpty()){
            return null;
        }else {
            return configs.get(0);
        }
    }
}
