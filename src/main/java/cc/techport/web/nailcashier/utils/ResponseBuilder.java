package cc.techport.web.nailcashier.utils;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class ResponseBuilder {

    private static final Gson gson = new Gson();

    private Map<String, Object> respMap;

    private ResponseBuilder(String respCode, String respMsg) {
        respMap = new HashMap<>();
        respMap.put("respCode", respCode);
        respMap.put("respMsg", respMsg);
    }

    public static ResponseBuilder builder() {
        return new ResponseBuilder("", "");
    }

    public ResponseBuilder respMsg(String respMsg) {
        respMap.put("respMsg", respMsg);
        return this;
    }

    public ResponseBuilder respCode(String respCode) {
        respMap.put("respCode", respCode);
        return this;
    }

    public ResponseBuilder property(String key, Object object) {
        respMap.put(key, object);
        return this;
    }

    public String asJson() {
        return gson.toJson(respMap);
    }

    public Map<String, Object> asMap() {
        return respMap;
    }

    public static String mapToJson(Map<String,Object> map){
        return gson.toJson(map);
    }
}
