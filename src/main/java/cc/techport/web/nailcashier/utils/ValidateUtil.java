package cc.techport.web.nailcashier.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUtil {
    public static boolean isWord(String s){
        if(StringUtils.isBlank(s))
            return false;

        String wordRegex = "(\\w)+";
        Pattern pattern = Pattern.compile(wordRegex);
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
}
