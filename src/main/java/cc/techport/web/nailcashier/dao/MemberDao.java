package cc.techport.web.nailcashier.dao;

import cc.techport.web.nailcashier.model.Member;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberDao {

    void createMember(Member member);

    void updateMemberInfo(Member member);

    void deleteMember(Member member);

    List<Member> getAllMembers();

    List<Member> getMemberById(@Param("memberId") Integer memberId);

    List<Member> getMemberByFullName(@Param("fullName") String fullName);

    List<Member> getMemberByMobile(@Param("mobile") String mobile);

    void fundMember(@Param("memberId") Integer memberId,@Param("amount") double amount);

    void chargeMember(@Param("memberId") Integer memberId,@Param("amount") double amount);

}
