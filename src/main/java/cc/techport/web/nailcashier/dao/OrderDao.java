package cc.techport.web.nailcashier.dao;

import cc.techport.web.nailcashier.dto.OrderType;
import cc.techport.web.nailcashier.model.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderDao {

    void createOrder(Order order);

    void updateOrderStatus(Order order);

    void updateOrderComment(Order order);

    Double getRevenue(@Param("startTime")String startTime,
                      @Param("endTime") String endTime,
                      @Param("orderType") String orderType);

    Double getCash(@Param("startTime")String startTime,
                   @Param("endTime")String endTime,
                   @Param("orderType") String orderType);

    Double getBalance(String startTime, String endTime, String orderType);

    List<Order> getOrderById(@Param("orderId") Integer orderId);

    List<Order> getOrderByTime(@Param("startTime") String startTime,
                               @Param("endTime") String endTime);

    List<Order> getOrderByMember(@Param("startTime") String startTime,
                                 @Param("endTime") String endTime,
                                 @Param("memberId")Integer memberId);

    List<Order> queryOrder(@Param("startTime") String startTime,
                           @Param("endTime") String endTime,
                           @Param("orderType") String orderType,
                           @Param("memberId")Integer memberId,
                           @Param("employeeId")Integer employeeId
                           );

}
