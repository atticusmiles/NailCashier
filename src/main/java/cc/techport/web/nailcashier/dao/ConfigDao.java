package cc.techport.web.nailcashier.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ConfigDao {

    void createConfig(@Param("key") String key,@Param("value") String value);

    void deleteConfig(@Param("key") String key);

    void updateConfig(@Param("key") String key,@Param("value") String value);

    List<String> getConfig(@Param("key") String key);

}
