package cc.techport.web.nailcashier.dao;

import cc.techport.web.nailcashier.model.Good;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoodDao {

    void createGood(Good good);

    void updateGoodInfo(Good good);

    void deleteGood(Good good);

    List<Good> getAllGoods();

    void setGoodStock(Good good);

    List<Good> getGoodById(@Param("goodId") Integer goodId);

}
