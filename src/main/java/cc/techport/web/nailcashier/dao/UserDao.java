package cc.techport.web.nailcashier.dao;

import cc.techport.web.nailcashier.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {

    void createUser(User user);

    void updateUserInfo(User user);

    void updateUserPwd(User user);

    void deleteUser(User user);

    List<User> getAllUsers();

    List<User> getUserByUsername(@Param("username") String username);

    List<User> getUserByUserId(@Param("userId") Integer userId);

    Integer isPwdAMatch(@Param("username") String username, @Param("password") String encryptedPwd);
}
