package cc.techport.web.nailcashier.dao;

import cc.techport.web.nailcashier.model.AuthGroup;
import cc.techport.web.nailcashier.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AuthGroupDao {

    void createAuthGroup(AuthGroup authGroup);

    void updateAuthGroup(AuthGroup authGroup);

    void deleteAuthGroup(AuthGroup authGroup);

    List<AuthGroup> getAllAuthGroups();

    List<AuthGroup> getAuthGroupByName(@Param("groupName") String groupName);

    List<AuthGroup> getAuthGroupByGroupId(@Param("groupId") Integer groupId);
}
