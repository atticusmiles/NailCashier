package cc.techport.web.nailcashier.dao;

import cc.techport.web.nailcashier.model.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeDao {

    void createEmployee(Employee employee);

    void updateEmployee(Employee employee);

    void deleteEmployee(Employee employee);

    List<Employee> getAllEmployees();

    List<Employee> getEmployeeById(@Param("employeeId") Integer employeeId);
}
