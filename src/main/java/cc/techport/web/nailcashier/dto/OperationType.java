package cc.techport.web.nailcashier.dto;

public enum OperationType {
    REFUND,
    DEPOSIT,
    PURCHASE,
    SERVICE
}
