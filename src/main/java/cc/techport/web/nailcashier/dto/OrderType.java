package cc.techport.web.nailcashier.dto;

public enum OrderType {
    DEPOSIT,
    PURCHASE
}
