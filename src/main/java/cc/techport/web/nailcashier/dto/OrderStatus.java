package cc.techport.web.nailcashier.dto;

public enum OrderStatus {
    VALID,
    REVOKED
}
