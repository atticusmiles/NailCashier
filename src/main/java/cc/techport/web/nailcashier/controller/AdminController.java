package cc.techport.web.nailcashier.controller;

import cc.techport.web.nailcashier.business.AuthBusiness;
import cc.techport.web.nailcashier.business.UserBusiness;
import cc.techport.web.nailcashier.model.AuthGroup;
import cc.techport.web.nailcashier.model.User;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    private UserBusiness userBusiness;
    @Autowired
    private AuthBusiness authBusiness;

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST},value = "/createUser")
    String createUser(@RequestBody User user) {
        return ResponseBuilder.mapToJson(userBusiness.createUser(user));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/updateUserInfo")
    String updateUserInfo(@RequestBody User user) {
        return ResponseBuilder.mapToJson(userBusiness.updateUserInfo(user));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/updateUserPwd")
    String updateUserPwd(@RequestBody User user) {
        return ResponseBuilder.mapToJson(userBusiness.updateUserPwd(user));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/deleteUser")
    String deleteUser(@RequestBody User user) {
        return ResponseBuilder.mapToJson(userBusiness.deleteUser(user));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getAllUsers")
    String getAllUsers() {
        return ResponseBuilder.mapToJson(userBusiness.getAllUsers());
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getAllAuthGroups")
    String getAllAuthGroups() {
        return ResponseBuilder.mapToJson(authBusiness.getAllAuthGroups());
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/createAuthGroup")
    String createAuthGroup(@RequestBody AuthGroup authGroup) {
        return ResponseBuilder.mapToJson(authBusiness.createAuthGroup(authGroup));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/updateAuthGroup")
    String updateAuthGroup(@RequestBody AuthGroup authGroup) {
        return ResponseBuilder.mapToJson(authBusiness.updateAuthGroup(authGroup));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/deleteAuthGroup")
    String deleteAuthGroup(@RequestBody AuthGroup authGroup) {
        return ResponseBuilder.mapToJson(authBusiness.deleteAuthGroup(authGroup));
    }
}
