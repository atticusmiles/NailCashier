package cc.techport.web.nailcashier.controller;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/config")
public class ConfigController {

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getDiscountList")
    public String getDiscount(){
        List<Integer> discounts = new ArrayList<Integer>();
        discounts.add(100);
        discounts.add(95);
        discounts.add(90);
        discounts.add(85);
        discounts.add(80);
        discounts.add(75);
        discounts.add(70);
        discounts.add(65);
        discounts.add(60);
        discounts.add(55);
        discounts.add(50);

        return ResponseBuilder.builder()
                .respCode(Constants.RESPCODE_SUCCESS)
                .respMsg("Done")
                .property("discounts", discounts)
                .asJson();
    }

}
