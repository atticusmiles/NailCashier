package cc.techport.web.nailcashier.controller;

import cc.techport.web.nailcashier.business.MemberBusiness;
import cc.techport.web.nailcashier.model.Member;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value = "/member")
public class MemberController {

    public static final Log LOGGER = LogFactory.getLog(MemberController.class);

    @Autowired
    private MemberBusiness memberBusiness;

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/createMember")
    public String createMember(@RequestBody Member member) {
        Map<String, Object> resp = memberBusiness.createMember(member);
        return ResponseBuilder.mapToJson(resp);
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/updateMemberInfo")
    public String updateMemberInfo(@RequestBody Member member) {
        Map<String, Object> resp = memberBusiness.updateMemberInfo(member);
        return ResponseBuilder.mapToJson(resp);
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/deleteMember")
    public String deleteMember(@RequestBody Member member) {
        Map<String, Object> resp = memberBusiness.deleteMember(member);
        return ResponseBuilder.mapToJson(resp);
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/fundMember")
    public String fundMember(@RequestBody Member member) {
        Map<String, Object> resp = memberBusiness.fundMember(member);
        return ResponseBuilder.mapToJson(resp);
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getAllMembers")
    public String getAllMembers() {
        Map<String, Object> resp = memberBusiness.getAllMembers();
        return ResponseBuilder.mapToJson(resp);
    }
}
