package cc.techport.web.nailcashier.controller;

import cc.techport.web.nailcashier.Constants;
import cc.techport.web.nailcashier.business.LoginBusiness;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import com.google.gson.Gson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
public class LoginController {
    public static final Log LOGGER = LogFactory.getLog(LoginController.class);

    @Autowired
    private LoginBusiness loginBusiness;

    public static class LoginForm {
        private String username;
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/login")
    public String login(@RequestBody LoginForm loginForm, HttpServletResponse response) {
        String authKey;
        Map<String, Object> resp = loginBusiness.doLogin(loginForm);
        if (Constants.RESPCODE_SUCCESS.equals(resp.get("respCode"))) {
            authKey = resp.get("authKey").toString();
            Cookie cookie = new Cookie("AuthKey", authKey);
            cookie.setHttpOnly(true);
            cookie.setMaxAge(48*60*60);
            cookie.setPath("/");
            response.addCookie(cookie);
        }
        resp.remove("authKey");
        return ResponseBuilder.mapToJson(resp);
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/logout")
    public String logout(HttpServletResponse response) {
        String authKey;
        Map<String, Object> resp = loginBusiness.doLogout();

        authKey = resp.get("authKey").toString();
        Cookie cookie = new Cookie("AuthKey", authKey);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
        resp.remove("authKey");

        return ResponseBuilder.mapToJson(resp);
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getUserInfo")
    public String getUserInfo(@CookieValue(Constants.AUTHKEY_COOKIE_NAME) String authKey, HttpServletResponse response) {
        Map<String, Object> resp = loginBusiness.getUserInfo(authKey);
        return ResponseBuilder.mapToJson(resp);
    }

}
