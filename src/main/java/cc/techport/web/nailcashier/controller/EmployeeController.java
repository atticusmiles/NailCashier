package cc.techport.web.nailcashier.controller;


import cc.techport.web.nailcashier.business.EmployeeBusiness;
import cc.techport.web.nailcashier.model.Employee;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    private EmployeeBusiness employeeBusiness;

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/createEmployee")
    String createEmployee(@RequestBody Employee employee) {
        return ResponseBuilder.mapToJson(employeeBusiness.createEmployee(employee));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/updateEmployee")
    String updateEmployee(@RequestBody Employee employee) {
        return ResponseBuilder.mapToJson(employeeBusiness.updateEmployee(employee));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/deleteEmployee")
    String deleteEmployee(@RequestBody Employee employee) {
        return ResponseBuilder.mapToJson(employeeBusiness.deleteEmployee(employee));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getAllEmployees")
    String getAllEmployees() {
        return ResponseBuilder.mapToJson(employeeBusiness.getAllEmployees());
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getEmployeeById")
    String getEmployeeById(Employee employee) {
        return ResponseBuilder.mapToJson(employeeBusiness.getEmployeeById(employee));
    }

}
