package cc.techport.web.nailcashier.controller;

import cc.techport.web.nailcashier.business.OrderBusiness;
import cc.techport.web.nailcashier.model.Employee;
import cc.techport.web.nailcashier.model.Order;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderBusiness orderBusiness;

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/doPurchase")
    String doPurchase(@RequestBody OrderBusiness.PurchaseOrderInfo orderInfo) {
        return ResponseBuilder.mapToJson(orderBusiness.createPurchaseOrder(orderInfo));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/doFund")
    String doFund(@RequestBody OrderBusiness.FundOrderInfo orderInfo) {
        return ResponseBuilder.mapToJson(orderBusiness.createFundOrder(orderInfo));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/doRevoke")
    String doRevoke(@RequestBody Order order) {
        return ResponseBuilder.mapToJson(orderBusiness.revokeOrder(order));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getOrderByTime")
    String getOrderByTime(String startTime, String endTime) {
        return ResponseBuilder.mapToJson(orderBusiness.getOrderByTime(startTime,endTime));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getOrderByMember")
    String getOrderByMember(String startTime, String endTime, Integer memberId) {
        return ResponseBuilder.mapToJson(orderBusiness.getOrderByMember(startTime,endTime,memberId));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/updateOrderComments")
    String updateOrderComments(@RequestBody Order order) {
        return ResponseBuilder.mapToJson(orderBusiness.updateOrderComments(order));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/updateOrderStatus")
    String updateOrderStatus(@RequestBody Order order) {
        return ResponseBuilder.mapToJson(orderBusiness.updateOrderStatus(order));
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/queryOrder")
    String queryOrder(@RequestBody OrderBusiness.QueryOrderInfo queryOrderInfo) {
        return ResponseBuilder.mapToJson(orderBusiness.queryOrder(queryOrderInfo));
    }
}
