package cc.techport.web.nailcashier.controller;

import cc.techport.web.nailcashier.business.GoodBusiness;
import cc.techport.web.nailcashier.model.Good;
import cc.techport.web.nailcashier.utils.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value = "/good")
public class GoodController {

    @Autowired
    private GoodBusiness goodBusiness;

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/createGood")
    public String createGood(@RequestBody Good good) {
        Map<String, Object> resp = goodBusiness.createGood(good);
        return ResponseBuilder.mapToJson(resp);
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/updateGoodInfo")
    public String updateGoodInfo(@RequestBody Good good) {
        Map<String, Object> resp = goodBusiness.updateGoodInfo(good);
        return ResponseBuilder.mapToJson(resp);
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/deleteGood")
    public String deleteGood(@RequestBody Good good) {
        Map<String, Object> resp = goodBusiness.deleteGood(good);
        return ResponseBuilder.mapToJson(resp);
    }

    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/getAllGoods")
    public String getAllGoods() {
        Map<String, Object> resp = goodBusiness.getAllGoods();
        return ResponseBuilder.mapToJson(resp);
    }


    @ResponseBody
    @RequestMapping(method = {RequestMethod.POST}, value = "/setGoodStock")
    public String setGoodStock(@RequestBody Good good) {
        Map<String, Object> resp = goodBusiness.setGoodStock(good);
        return ResponseBuilder.mapToJson(resp);
    }

}
