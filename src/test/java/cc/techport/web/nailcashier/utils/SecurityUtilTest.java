package cc.techport.web.nailcashier.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class SecurityUtilTest {
    public static final Log LOG = LogFactory.getLog(SecurityUtilTest.class);

    @Test
    public void testMD5(){
        String plainText = "123456";

        LOG.info("plain text: ["+ plainText + "]");
        LOG.info("md5: ["+ SecurityUtil.makeMD5(plainText)+ "]");
    }
}