package cc.techport.web.nailcashier.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidateUtilTest {
    @Test
    public void isWord() throws Exception {
        assertEquals(true, ValidateUtil.isWord("howfun123"));
        assertEquals(true, ValidateUtil.isWord("admin1"));
        assertEquals(false, ValidateUtil.isWord("howfu-*n123"));
    }

}